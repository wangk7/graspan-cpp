#include "run_pre.h"

int run_preprocessing(Context &context) {
	cout << "##### PREPROCESSING START #####" << endl;

	clock_t begin, end;
	//new Preproc
	
	begin = clock();

	Preproc_new preN(context);
	preN.setMapInfo(context.grammar.getMapInfo(), context.grammar.getErules());
	preN.countNum(context);
	preN.setVIT(context);
	context.ddm.setNumPartition(context.getNumPartitions());
	context.ddm.reSize();
	preN.saveData(context);
	preN.mergePart(context);
	end = clock();
	cout << "NEWPREPROC TIME: " << ((end - begin) / CLOCKS_PER_SEC) << std::endl;
	return 0;
}
